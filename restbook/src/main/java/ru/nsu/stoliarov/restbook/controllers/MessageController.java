package ru.nsu.stoliarov.restbook.controllers;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;
import ru.nsu.stoliarov.restbook.domain.Message;
import ru.nsu.stoliarov.restbook.repository.MessageRepo;

import java.util.List;

@RestController
@RequestMapping("message")
@Log4j2
public class MessageController {
	private final MessageRepo messages;
	
	@Autowired
	public MessageController(MessageRepo messages) {
		this.messages = messages;
	}
	
	/** дефолтный GET запрос*/
	@GetMapping
	public List<Message> list(
			@RequestParam(name = "page", required = false, defaultValue = "0") String page,
			@RequestParam(name = "size", required = false, defaultValue = "10") String size
	) {
		Pageable pageable = PageRequest.of(Integer.valueOf(page), Integer.valueOf(size), Sort.Direction.ASC, "id");
		
		// т.к. это rest контроллер, то он не ищет файл с именем = возвращаемое_значение,
		// а выводит содержимое возвращаемого значения
		return messages.findAll(pageable).getContent();
	}
	
	/** Returns number of entity in the data base */
	@GetMapping ("size")
	public Long getSizeOfDB() {
		return messages.count();
	}
	
	/** GET by id */
	@GetMapping("id/{id}") // т.е. для запросов message/какой-то id будет вызываться этот метод
	// @PathVariable("id") благодаря этой подсказке ("id") spring будет искать сущность message по id из URL
	public Message getById(@PathVariable("id") Message messageFromDB) {
		return messageFromDB;
	}
	
	/** GET by text */
	@GetMapping ("text/{text}")
	public List<Message> getByText(
			@PathVariable String text,
			@RequestParam(name = "page", required = false, defaultValue = "0") String page,
			@RequestParam(name = "size", required = false, defaultValue = "10") String size
	) {
		Pageable pageable = PageRequest.of(
				Integer.valueOf(page), Integer.valueOf(size), Sort.Direction.ASC, "id"
		);
		
		return messages.findByTextContaining(text, pageable).getContent();
	}
	
	/** POST (добавление записи) */
	@PostMapping
	public Message create(@RequestBody Message message) {
		return messages.save(message);
	}
	
	/** PUT (обновление записи) */
	@PutMapping("{id}")
	public Message update(
			@PathVariable("id") Message messageFromDB, // по id из url spring найдет в БД объект Message
			@RequestBody Message messageFromUser) // из json сообщения spring сделает объект Message
	{
		// копирует все из FromUser в FromDB кроме id
		BeanUtils.copyProperties(messageFromUser, messageFromDB, "id");
		
		return messages.save(messageFromDB);
	}
	
	/** DELETE */
	@DeleteMapping ("{id}")
	public void delete(@PathVariable("id") Message message) {
		messages.delete(message);
	}
}
