package ru.nsu.stoliarov.restbook.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.nsu.stoliarov.restbook.domain.Message;

// Message - тип хранимых в бд данных, Integer - тип ключей
public interface MessageRepo extends JpaRepository<Message, Integer> {
	
	Page<Message> findAll(Pageable pageable);
	
	Page<Message> findByTextContaining(String text, Pageable pageable);
	
}
