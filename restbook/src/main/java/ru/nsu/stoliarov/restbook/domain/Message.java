package ru.nsu.stoliarov.restbook.domain;

import javax.persistence.*;

@Entity // для сущностей создается таблица в БД
@Table
public class Message {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	private String text;
	
	public Message() {}
	
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getText() {
		return text;
	}
	
	public void setText(String text) {
		this.text = text;
	}
	
	@Override
	public String toString() {
		return "Message(id=" + this.getId() + ", text=" + this.getText() + ")";
	}
}
