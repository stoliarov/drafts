package ru.nsu.stoliarov.restbook.domain;

public enum Role {
	USER,
	ADMIN,
}
