package ru.nsu.stoliarov.restbook;

import com.zaxxer.hikari.HikariConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestbookApplication {
	
	public static void main(String[] args) {
		HikariConfig hikariConfig = new HikariConfig();
		hikariConfig.setMinimumIdle(5);
		hikariConfig.setMaximumPoolSize(10);
		
		SpringApplication.run(RestbookApplication.class, args);
		
	}
}
