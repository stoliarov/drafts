package ru.nsu.stoliarov.restbook.config;

import com.netflix.concurrency.limits.servlet.ConcurrencyLimitServletFilter;
import com.netflix.concurrency.limits.servlet.ServletLimiterBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.servlet.Filter;

@Configuration
public class FilterConfig {
	@Bean
	public Filter getFilter() {
		return new ConcurrencyLimitServletFilter(new ServletLimiterBuilder()
				.partitionByHeader("MyPartition", c -> c
						.assign("live", 0.5)
						.assign("batch", 0.5))
				.build()
		);
	}
}
