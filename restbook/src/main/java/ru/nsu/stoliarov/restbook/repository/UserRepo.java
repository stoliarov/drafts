package ru.nsu.stoliarov.restbook.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.nsu.stoliarov.restbook.domain.User;

public interface UserRepo extends JpaRepository<User, String> {
}
