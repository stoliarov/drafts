create table SPRING_SESSION
(
  PRIMARY_ID            char(36)     not null
    primary key,
  SESSION_ID            char(36)     not null,
  CREATION_TIME         bigint       not null,
  LAST_ACCESS_TIME      bigint       not null,
  MAX_INACTIVE_INTERVAL int          not null,
  EXPIRY_TIME           bigint       not null,
  PRINCIPAL_NAME        varchar(300) null,
  constraint SPRING_SESSION_IX1
  unique (SESSION_ID)
)
  engine = InnoDB;

create index SPRING_SESSION_IX1
  on SPRING_SESSION (PRINCIPAL_ID);

create index SPRING_SESSION_IX2
  on SPRING_SESSION (EXPIRY_TIME);

create index SPRING_SESSION_IX3
  on SPRING_SESSION (PRINCIPAL_NAME);

CREATE TABLE SPRING_SESSION_ATTRIBUTES (
  session_primary_id CHAR(36) NOT NULL
  CONSTRAINT spring_session_attributes_fk
  REFERENCES spring_session
  ON DELETE CASCADE,
  attribute_name VARCHAR(200) NOT NULL,
  attribute_bytes BYTEA NOT NULL,
  CONSTRAINT spring_session_attributes_pk
  PRIMARY KEY (session_primary_id, attribute_name)
);