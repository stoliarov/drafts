package ru.nsu.stoliarov.guestbook1.app.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity  // для сущности создастся таблица в бд
public class Message {
	@Id  // это будет id
	// фраемворк пусть сам решает в каком виде и в каком порядке будут генерироваться эти id:
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	
	private String text;
	private String tag;
	
	// Обязательно имеем конструткор без параметров, иначе спринг не сможет сам создавать объект!!!
	// (Для всех Entity классов)
	public Message() {}
	
	public Message(String text, String tag) {
		this.text = text;
		this.tag = tag;
	}
	
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getText() {
		return text;
	}
	
	public void setText(String text) {
		this.text = text;
	}
	
	public String getTag() {
		return tag;
	}
	
	public void setTag(String tag) {
		this.tag = tag;
	}
}
