package ru.nsu.stoliarov.guestbook1.app.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.nsu.stoliarov.guestbook1.app.domain.Message;
import ru.nsu.stoliarov.guestbook1.app.repository.MessageRepo;

import java.util.List;
import java.util.Map;

@Controller // Контроллер по указанному пути (все, начиная с домешнего)
// ("/greeting", "/") слушает запросы от пользователя
public class AppController {
	@Autowired
	private MessageRepo messageRepo;
	
	@GetMapping("/greeting")
	public String greeting(
			@RequestParam(name = "name", required = false, defaultValue = "World") String name,
			Map<String, Object> model
	) {
		// в модель складываем данные, которые хотим вернуть пользователю ("name" - ключ параметра)
		// в mustache файле это будет выгребаться для html вывода
		model.put("name", name);
		
		// возвращаем имя файла спринговому контейнеру, который мы хотим отобразить
		// все эти файлы должны лежать в resource.templates или в дочерних
		return "greeting";  // greeting.mustache
	}
	
	@GetMapping
	public String home(Map<String, Object> model) {
		Iterable<Message> messages = messageRepo.findAll();
		model.put("messages", messages);
		
		return "home";  // home.mustache
	}
	
	@PostMapping("add")
	public String add(@RequestParam String text, @RequestParam String tag, Map<String, Object> model) {
		// RequestParam выдергивает значение запроса либо из URL (GetMapping),
		// либо из формы (post)
		
		// сохраняем в базу
		Message message = new Message(text, tag);
		messageRepo.save(message);
		
		// берем из базы для вывода
		Iterable<Message> messages = messageRepo.findAll();
		model.put("messages", messages);
		
		return "home";
	}
	
	@PostMapping("filterMapping")
	public String filter(@RequestParam String filter, Map<String, Object> model) {
		Iterable<Message> messages;
		
		if(null == filter || filter.isEmpty()) {
			messages = messageRepo.findAll();
		} else {
			messages = messageRepo.findByTag(filter);
		}
		
		model.put("messages", messages);
		
		return "home";
	}
}
