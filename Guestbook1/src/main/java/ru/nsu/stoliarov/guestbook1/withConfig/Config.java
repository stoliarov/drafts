package ru.nsu.stoliarov.guestbook1.withConfig;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("ru.nsu.stoliarov.guestbook1.withAnnotation") // дополнительно добавляет автоматическое
// сканирование этого пакета в поисках @Component
public class Config {
	@Bean ("my Object 1")
	public Object getObject() {
		return new Object();
	}
	
	// в этом случае имя бина = имя МЕТОДА
	@Bean
	public String getString() {
		// можем сначала поделать с этим объектом всякое, например задать имя
		// в параметрах к методу можно указать другой бин, если хотим, например, юзать его имя
		return new String();
	}
}
