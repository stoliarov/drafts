package ru.nsu.stoliarov.guestbook1.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application {
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
}

// TODO Внимание вопрос: как наш MessageRepo попадает в контекст?

// EnableAutoConfiguration разрешает спрингу добавлять в контекст абсолютно
// все наши классы?

// или т.к. CrudRepository наследуется от Repository, а Repository имеет аннотацию @Indexed,
// к которой автоматически подставляется @Component?