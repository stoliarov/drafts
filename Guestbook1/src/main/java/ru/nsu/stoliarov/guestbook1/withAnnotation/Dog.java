package ru.nsu.stoliarov.guestbook1.withAnnotation;

import org.springframework.stereotype.Component;

@Component
public class Dog {
	public Dog() {}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	private String name = "Dog_name";
}
