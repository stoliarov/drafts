package ru.nsu.stoliarov.guestbook1.withAnnotation;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

// это вариант с автматическим сканированием
public class MainAnnotation {
	static public void main(String argv[]) {
		// this spring context will search own a beans in the specified package by an annotations
		ApplicationContext context = new AnnotationConfigApplicationContext
				("ru.nsu.stoliarov.guestbook1.withAnnotation");
		
		Cat cat = context.getBean(Cat.class);
		Dog dog = (Dog) context.getBean("dog");
		Parrot parrot = context.getBean("parrot-kesha", Parrot.class);
		
		System.out.println(parrot.getName());
		System.out.println(cat.getName());
		System.out.println(dog.getName());
		
		Cat cat1 = context.getBean(Cat.class);
		cat1.setName("Caaat");
		cat.setName("Other cat");
		
		System.out.println(cat.getName() + " " + cat1.getName());
	}
}
