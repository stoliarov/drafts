package ru.nsu.stoliarov.guestbook1.withAnnotation;

import org.springframework.stereotype.Component;

@Component("parrot-kesha")
public class Parrot {
	public Parrot() {}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	private String name = "Parrot_name";
}
