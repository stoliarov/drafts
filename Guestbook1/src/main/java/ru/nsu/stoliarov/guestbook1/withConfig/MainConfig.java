package ru.nsu.stoliarov.guestbook1.withConfig;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.nsu.stoliarov.guestbook1.withAnnotation.Cat;

// вариант с ручным java-конфигом
public class MainConfig {
	static public void main(String argv[]) {
		// this spring context will search own a beans in the specified package by an annotations
		// Also i can send to parameters a few Configuration - comma-separated list
		// Or package that have all the Configurations ("ru/nsu/stoliarov/guestbook1/...")
		// пакет можно также через запятую, или можно указать общий для них пакет
		ApplicationContext context = new AnnotationConfigApplicationContext(Config.class);
		
		Object object = context.getBean("my Object 1");
		Object object1 = context.getBean("my Object 1");
		String string = context.getBean(String.class);
		
		Cat cat = context.getBean(Cat.class);
		System.out.println(cat.getName());
	}
}
