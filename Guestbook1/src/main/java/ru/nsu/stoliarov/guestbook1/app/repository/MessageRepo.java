package ru.nsu.stoliarov.guestbook1.app.repository;

import org.springframework.data.repository.CrudRepository;
import ru.nsu.stoliarov.guestbook1.app.domain.Message;

import java.util.List;

// репозиторий - обмен данными с бд
public interface MessageRepo extends CrudRepository<Message, Long> {
	
	// можно поменять <Message> на name (id) например (и в home.mustache тоже)
	// чтобы выводить только имена (id) из бд
	List<Message> findByTag(String tag);
	
}
