package ru.nsu.stoliarov.guestbook1;

import javax.xml.transform.Result;
import java.sql.*;

public class Main {
	static public void main(String argv[]) {
		String userName = "stoliarov";
		String password = "";
		String url = "jdbc:mariadb://localhost:3306/myFirstBase";
		
		try {
			Class.forName("org.mariadb.jdbc.Driver");
			Connection connection = DriverManager.getConnection(url, userName, password);
			
			System.out.println("We are connected!");
			
			// sql scripts:
			Statement statement = connection.createStatement();
			statement.executeUpdate("create table if not exists entries(" +
					"id int primary key auto_increment," +
					"message text not null" +
					");");
			
			statement.executeUpdate("insert into entries (message) values('First message');");
			
			// out from an inner data of the entries table
			ResultSet resultSet = statement.executeQuery("select * from entries");
			while(resultSet.next()) {
				System.out.println(resultSet.getString("message"));
				System.out.println("-------------------");
			}
			
			String id = "1";
			// the protection against a sql injections
			PreparedStatement preparedStatement =
					connection.prepareStatement("select * from entries where id = ?;");
			preparedStatement.setString(1, id);
			
			ResultSet resultSet2 = preparedStatement.executeQuery();
			while(resultSet2.next()) {
				System.out.println(resultSet2.getString("message"));
				System.out.println("===================");
			}
			
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
